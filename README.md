# OpenML dataset: olindda_outliers

https://www.openml.org/d/42793

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Outliers data set extracted from the Illustration (Fig. 3) in &quot;Novelty detection with application to data
streams&quot;

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42793) of an [OpenML dataset](https://www.openml.org/d/42793). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42793/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42793/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42793/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

